= Gateway Procedures for Refugee Cash and Medical Assistance
:revision-date: 09/05/23
:next-review: 09/05/25

== Refugee Cash Assistance (RCA)

Standard deduction for TANF applied for Refugee Cash Assistance.
The earned income deduction for TANF must be applied to recipients of Refugee Cash Assistance.

=== Gateway Procedure

Enter Earned Income budget by computing monthly gross.
Enter the gross amount into Earned Income screen.
EDBC process will run and apply appropriate Standard Deduction.
Document on Case note how you arrived at this gross income amount.

In addition to RCA, if the person receives or applies for SNAP, the above income source must be reflected in the SNAP budget.
Gateway will budget the RCA and the FS cases correctly.

== Reception and Placement Cash Payments (RCA)

Any cash grant received by an applicant or recipient under the Department of State or Department of Justice Reception and Placement programs cannot be considered in determining eligibility for RCA.
This income, however, must be counted in the related SNAP budget.

=== Gateway Procedure

Add Reception and Placement Cash Payment to SNAP budget.

== Proration of RCA Benefits

RCA benefits must be prorated from the date of application, provided all program requirements were met on that date.
If not, RCA benefits must be prorated from the date the EDBC process complete.

=== Gateway Procedure

Enter Date of Application and Gateway system will determine the Proration of benefits.

== Refugee Medical Assistance (RMA)

Refugee Medical Assistance (RMA) is available only to refugees who are not eligible for regular Medicaid or Peach Care for Kids.
Medicaid eligibility must be determined in the correct order, beginning with Low Income Medicaid (LIM).

=== Gateway Procedure

An applicant eligible for any kind of class of Medical assistance will be determined by EDBC process and will be placed in appropriate Class of Medical Assistance category.

== Income and Resource Eligibility for RMA

Eligibility for RMA will be determined on the basis of a refugee applicants’ income and resources on the date of application.

=== Gateway Procedure

No unique Gateway procedures required.

== Earned Income while on RMA

If a refugee who is receiving RMA receives earnings from employment, the earnings shall not affect the refugee's continued RMA eligibility (RMA will change to Transitional RMA for the remainder of the 12 month eligibility).

=== Gateway Procedure

If a refugee is receiving Refuge Cash and Medical Assistance the case will automatically trickle to Refugee Transitional Medical Assistance when earnings are entered that make the assistance unit ineligible.
All adults (except pregnant women) should be cascaded to Refugee Transitional Medical Assistance, and pregnant women and children should be put on RSM, if eligible.

== Earned Income while on Low Income Medicaid (LIM)

Refugees who lose their eligibility for Low Income Medicaid because of earnings from employment will be cascaded to Transitional Medical Assistance via the EDBC process.

=== Gateway Procedure

If a refugee family is receiving LIM, the case will automatically cascade to Transitional Medical Assistance (TMA) or (F07) for up to 12 months if requirements are met.
If earnings higher than the LIM limits are entered and the family has received LIM in 3 of the 6 months preceding the first month of LIM eligibility, the EDBC will determine appropriate class of Medical Assistance for all HH members.

== Income Ceiling Limit for RMA

The Federal Poverty Limit (FPL) used as the RMA financial standard is 200%.

=== Gateway Procedure

Enter gross income and Gateway system will determine eligibility.

== Treatment of Reception and Placement/Match Grant Payments

Cash assistance payments made under the Department of State's Reception and Placement program, or the Matching Grant program will not be considered in the determination of eligibility for RMA.
This income, however, must be counted in the related Food Stamp budget.

=== Gateway Procedure

Add Reception and Placement Cash Payment to SNAP budget.

== RCA Denial or Termination in regard to RMA

Denial or termination from RCA will not cause denial of RMA benefits.

=== Gateway Procedure

No information input required.Gateway system will cascade Case to correct Class of Assistance.